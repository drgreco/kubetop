# kubetop

View Kubernetes nodes, pods, services, and deployments in a glance.

![Screenshot](https://raw.githubusercontent.com/siadat/kubetop/screenshot/screenshot.png)

## Install

    go get github.com/siadat/kubetop

## Usage

    kubetop

kubetop loads configs from ~/.kube/config
